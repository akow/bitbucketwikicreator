package bitbucketwikicreator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author kow
 */
public class BitbucketWikiCreator {

  public static String getProgName() {
    return new java.io.File(BitbucketWikiCreator.class.getProtectionDomain()
     .getCodeSource()
     .getLocation()
     .getPath())
     .getName();
  }

  public static final String EXTENSION_MD = ".md";
  public static final String HOME_WIKI = "Home.md";
  public static final String INDENTATION = "    ";
  private static final String HOME = "Home";
  private static final String SAMPLE_FILENAME = "bbwikisample";
  private static final String SAMPLE_STR = "home\n"
   + "Home/Application Manager\n"
   + "home/Multiplayer\n"
   + "home/Single Player\n"
   + "Home/Application\n"
   + "Home/Application/Game\n"
   + "Home/Application/Game/Entities\n"
   + "Home/Application/Game/World\n"
   + "Home/Application/Game/World/Objects\n"
   ;

  private static void createSample() {
    try {
      File fp = new File(SAMPLE_FILENAME);
      fp.delete();
      if (fp.createNewFile()) {
        PrintWriter writer = new PrintWriter(SAMPLE_FILENAME, "UTF-8");
        writer.print(SAMPLE_STR);
        writer.close();
      }
    } catch (IOException ex) {
      Logger.getLogger(BitbucketWikiCreator.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  private final String fullStr;
  private final List<String> allPages;

  public BitbucketWikiCreator(File fp) {
    String str = "";
    try {
      str = FileUtils.readFileToString(fp, Charsets.UTF_8);
    } catch (IOException ex) {
      Logger.getLogger(BitbucketWikiCreator.class.getName()).log(Level.SEVERE, null, ex);
    }
    this.fullStr = str;
    this.allPages = new ArrayList<String>(Arrays.asList(WordUtils.capitalize(str, new char[]{'/','\n'}).split("\\n")));

    for (String page : allPages) {
      this.createPage(page);
    }
  }

  private void createPage(String extName) {
    String name = extendedPageSplit(extName).get(extendedPageSplit(extName).size() - 1);

    if (name.compareToIgnoreCase(HOME) == 0) {
      name = HOME_WIKI;
    } else {
      name += EXTENSION_MD;
    }

    File f = new File(name);
    if (f.exists()) {
      f.delete();
    }

    try {
      if (!f.createNewFile()) {
        Logger.getLogger(BitbucketWikiCreator.class.getName()).log(Level.SEVERE, "Failed to create new file", new Exception(name));
      } else {
        PrintWriter writer = new PrintWriter(name, "UTF-8");
        writeLocation(writer, extName);
        writeTOC(writer);
        writeSubPages(writer, extName);
        writer.close();
      }
    } catch (IOException ex) {
      Logger.getLogger(BitbucketWikiCreator.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("Usage: " + getProgName() + " <JSON file>");
      System.out.println("Usage: " + getProgName() + " sample");
    } else {
      if (args[0].compareTo("sample")==0) {
        BitbucketWikiCreator.createSample();
      } else {
        BitbucketWikiCreator bwt = new BitbucketWikiCreator(new File(args[0]));
      }
    }
  }

  private void writeLocation(PrintWriter writer, String extName) {
    // Write Section path
    writer.print("You are here: ");
    List<String> ls = extendedPageSplit(extName);

    for (int i = 0; i < ls.size(); i++) {

      writer.print("[" + ls.get(i) + "](" + ls.get(i) + ")");

      if (i < ls.size() - 1) {
        writer.print(" -> ");
      }
    }
    writer.print("\n\n");
  }

  private void writeTOC(PrintWriter writer) {
    // Write Section Goto
    writer.println("Jump to...\n");
    writer.println("[TOC]");
    writer.println("");
  }

  private void writeSubPages(PrintWriter writer, String extName) {
    List<String> pages = new ArrayList<String>();
    for (String extPg : allPages) {
      if (extPg.contains(extName) && !extPg.equals(extName)) {

        pages.add(extPg);
      }
    }
    List<String> epsExtName = extendedPageSplit(extName);
    // Write Section Pages
    writer.println("### Subpages\n");
    for (String pg : pages) {

      List<String> epsPG = extendedPageSplit(pg);
      writer.println(StringUtils.repeat(INDENTATION, epsPG.size() - epsExtName.size() - 1) + "* [" + epsPG.get(epsPG.size() - 1) + "](" + epsPG.get(epsPG.size() - 1) + ")");
    }
    writer.println("");
  }

  private List<String> extendedPageSplit(String extName) {
    return new ArrayList<String>(Arrays.asList(extName.split("/")));
  }
}

